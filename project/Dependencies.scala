import sbt._

object Dependencies {
  lazy val magnolia = "com.propensive" %% "magnolia" % "0.1.0"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"
  lazy val shapeless = "com.chuusai" %% "shapeless" % "2.3.2"
  lazy val catsCore = "org.typelevel" %% "cats-core" % "1.0.0-MF"
}
