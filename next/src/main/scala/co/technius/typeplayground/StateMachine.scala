package co.technius.typeplayground.next

abstract class Door[State <: Door.State.DoorAny] {
  val state: State
}

object Door {
  object State extends Phantom {
    type DoorAny = this.Any
    type Open <: this.Any
    type Closed <: this.Any
    def any[S <: this.Any] = assume
  }

  def create: Door[State.Closed] = new Door[State.Closed] {
    val state = Door.State.any
  }

  def close(d: Door[State.Open]): Door[State.Closed] = new Door[State.Closed] {
    val state = Door.State.any
  }

  def open(d: Door[State.Closed]): Door[State.Open] = new Door[State.Open] {
    val state = Door.State.any
  }

  val myDoor = create
  val myDoor1 = open(myDoor)
  val myDoor2 = close(myDoor1)
}
