import org.scalatest._
import shapeless._
import shapeless.record._

import co.technius.typeplayground._

class DiffSpec extends FlatSpec with Matchers {
  "Diff" should "work" in {
    case class Foo(a: Int, b: String)
    val fooA = Foo(10, "asdf")
    val fooB = Foo(10, "bar")
    info(Diff.diff(fooA, fooB).mkString(","))
  }
}
