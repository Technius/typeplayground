import org.scalatest._

import co.technius.typeplayground._
import co.technius.typeplayground.derive.MagnoliaCsvWriter._

class MagnoliaCsvWriterSpec extends FlatSpec with Matchers {
  // "Magnolia csv writer derivation" should "serialize tuples" in {
  //   CsvWriter.write((1, 2, 3, 4, 5)) should be ("1,2,3,4,5")
  //   CsvWriter.write((1, true, "three")) should be ("1,true,three")
  //   CsvWriter.write(("foo", "bar", "baz")) should be ("foo,bar,baz")
  // }
  //
  // FIXME: magnolia doesn't seem to support tuples yet (if it ever will)

  "Magnolia csv writer derivation" should "serialize arbitrary case classes" in {
    case class Foo(bar: String, baz: Int, qux: Boolean)
    CsvWriter.write(Foo("abc", 1, false)) should be ("abc,1,false")
  }

  it should "serialize arbitrary sealed trait hierarchies" in {
    sealed trait Foo
    case class Bar(a: Int) extends Foo
    case class Baz(a: String, b: Int) extends Foo
    case class Qux(a: Boolean, b: Double) extends Foo

    val bar = Bar(1)
    val baz = Baz("abc", 1)
    val qux = Qux(false, 0.5)
    CsvWriter.write[Foo](bar) should be ("1")
    CsvWriter.write[Foo](baz) should be ("abc,1")
    CsvWriter.write[Foo](qux) should be ("false,0.5")
    CsvWriter.writeAll[Foo](Seq(bar, baz, qux)) should be (
    """1
    |abc,1
    |false,0.5""".stripMargin)
  }
}
