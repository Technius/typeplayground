import org.scalatest._

import co.technius.typeplayground._

class CsvWriterSpec extends FlatSpec with Matchers {
  "CSV writer" should "work" in {
    CsvWriter.write("foo") should be ("foo")
    CsvWriter.write(123) should be ("123")
    CsvWriter.write(true) should be ("true")
  }

  it should "serialize subtypes of Seq[T]" in {
    CsvWriter.write[Seq[String]](List("foo", "bar", "baz")) should be ("foo,bar,baz")
    CsvWriter.write(Vector("foo", "bar", "baz")) should be ("foo,bar,baz")
    CsvWriter.write(Seq("foo", "bar", "baz")) should be ("foo,bar,baz")
  }
}
