import org.scalatest._

import co.technius.typeplayground._

class RefinementsSpec extends FlatSpec with Matchers {
  "writeColor" should "accept rgb" in {
    // Refinements.writeColor("red") should be ("red")
    // Refinements.writeColor("blue") should be ("blue")
    // Refinements.writeColor("green") should be ("green")
  }

  "writeColor" should "not compile with other colors" in {
    """Refinements.writeColor("orange")""" shouldNot typeCheck
  }
}
