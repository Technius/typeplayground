package co.technius.typeplayground

import shapeless._
import shapeless.labelled.FieldType

object Diff {
  trait DiffChange {
    val field: Symbol
  }
  case class Change[A](field: Symbol, old: A, updated: A) extends DiffChange {
    override def toString = s"Change(${field.name}: $old => $updated)"
  }
  case class NoChange(field: Symbol) extends DiffChange {
    override def toString = s"NoChange(${field.name})"
  }

  def diff[T](old: T, updated: T)(implicit differ: Differ[T]) = differ.diff(old, updated)

  trait Differ[T] {
    def diff(old: T, updated: T): Seq[DiffChange]
  }
  object Differ {
    def apply[T](f: (T, T) => Seq[DiffChange]): Differ[T] = new Differ[T] {
      def diff(old: T, updated: T) = f(old, updated)
    }
  }

  implicit def genericDiffer[T, Repr](
    implicit g: LabelledGeneric.Aux[T, Repr],
    differ: Differ[Repr]): Differ[T] = {
    Differ((old, updated) => differ.diff(g.to(old), g.to(updated)))
  }

  implicit val hnilDiffer: Differ[HNil] = Differ((_,_) => Seq.empty)
  implicit def hconsDiffer[Key <: Symbol, H, T <: HList](
    implicit h: Differ[FieldType[Key, H]],
    t: Differ[T]
  ): Differ[FieldType[Key, H] :: T] =
    Differ { (old, updated) =>
      h.diff(old.head, updated.head) ++ t.diff(old.tail, updated.tail)
    }

  implicit def keyedDiffer[Key <: Symbol, A](
    implicit key: Witness.Aux[Key]
  ): Differ[FieldType[Key, A]] = Differ { (old, updated) =>
    if (old == updated) Seq(NoChange(key.value))
    else Seq(Change(key.value, old, updated))
  }
}
