package co.technius.typeplayground.derive

import magnolia._
import scala.language.experimental.macros
import scala.collection.immutable.ListMap

import co.technius.typeplayground.CsvWriter

/**
  * A version of CsvWriter, except derivation is performed by Magnolia
  */
object MagnoliaCsvWriter extends MagnoliaCsvWriterLowPriorityImplicits {
  implicit val derivation = new ContravariantDerivation[CsvWriter] {
    type Return = Seq[String]
    /**
      * Produces the returned type
      */
    def call[T](w: CsvWriter[T], value: T): Return = w.write(value)

    /**
      * Creates a CsvWriter for the given type
      */
    def construct[T](body: T => Return): CsvWriter[T] = CsvWriter(body)

    /**
      * Combines multiple returned values (e.g. in a case class)
      */
    def join(elements: List[Return]): Return =
      elements.flatten
  }
}

trait MagnoliaCsvWriterLowPriorityImplicits {
  /**
    * Generic derivation. Note: has to be low priority or it interferes with
    * the `implicit val derivation` above
    */
  implicit def generic[T]: CsvWriter[T] = macro Macros.magnolia[T, CsvWriter[_]]
}
