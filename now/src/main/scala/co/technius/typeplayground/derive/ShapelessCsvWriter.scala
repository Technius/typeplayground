package co.technius.typeplayground.derive

import shapeless._

import co.technius.typeplayground.CsvWriter

/**
  * Shapeless derivations for CsvWriter
  */
object ShapelessCsvWriter {
  /**
    * Instance of CsvWriter for tuples
    */
  implicit def pairWriter[P <: Product, Repr <: HList](
    implicit gen: Generic.Aux[P, Repr], wp: CsvWriter[Repr]): CsvWriter[P] =
    CsvWriter(p => wp.write(gen.to(p)))

  /**
    * Instance of CsvWriter for HNil, required for automatic derivation of
    * arbitrary case classes.
    */
  implicit val hnilWriter: CsvWriter[HNil] = CsvWriter(_ => Seq.empty)

  /**
    * Instance of CsvWriter for HLists, required for automatic derivation of
    * arbitrary case classes. Provides a writer for HLists containing types that
    * can be written.
    */
  implicit def hlistWriter[H, T <: HList](
    implicit wh: Lazy[CsvWriter[H]], wt: CsvWriter[T]): CsvWriter[H :: T] =
    CsvWriter { case h :: t => wh.value.write(h) ++ wt.write(t) }

  /**
    * Instance of CsvWriter for HNil, required for automatic derivation of
    * arbitrary sealed trait hierarchies.
    */
  implicit def cnilWriter: CsvWriter[CNil] = CsvWriter(_ => Seq.empty)

  /**
    * Instance of CsvWriter for coproducts, required for automatic derivation of
    * arbitrary sealed trait hierarchies.
    */
  implicit def coproductWriter[H, T <: Coproduct](
    implicit hw: Lazy[CsvWriter[H]], ht: CsvWriter[T]): CsvWriter[H :+: T] =
    CsvWriter {
      case Inl(h) => hw.value.write(h)
      case Inr(t) => ht.write(t)
    }

  /**
    * Provides automatic derivation of CsvWriter for arbitrary case classes and
    * sealed trait hierarchies.
    */
  implicit def genericWriter[T, Repr](
      implicit
      gen: Generic.Aux[T, Repr],
      w: CsvWriter[Repr]): CsvWriter[T] =
    CsvWriter(t => w.write(gen.to(t)))

}
