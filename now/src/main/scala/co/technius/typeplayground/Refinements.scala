package co.technius.typeplayground

import shapeless._
import shapeless.syntax.singleton._

object Refinements {
  val wRed = Witness("red")
  val wBlue = Witness("blue")
  val wGreen = Witness("green")

  type Color = wRed.T :+: wBlue.T :+: wGreen.T :+: CNil

  def writeColor(color: Color): String =
    color.unify

  writeColor(Coproduct[Color]("red".narrow))
}
