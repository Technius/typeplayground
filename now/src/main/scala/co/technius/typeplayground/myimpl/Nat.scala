package co.technius.typeplayground.myimpl

sealed trait Nat
sealed trait _0 extends Nat
sealed trait Succ[N <: Nat] extends Nat

sealed trait Add[N <: Nat, M <: Nat] {
  type Out <: Nat
}

object Add {
  type Aux[N <: Nat, M <: Nat, Out0 <: Nat] = Add[N, M] { type Out = Out0 }

  def apply[N <: Nat, M <: Nat](implicit add: Add[N, M]): Aux[N, M, add.Out] = add

  implicit def addIdentityRight[N <: Nat]: Aux[N, _0, N] = new Add[N, _0] {
    type Out = N
  }

  implicit def addIdentityLeft[N <: Nat]: Aux[_0, N, N] = new Add[_0, N] {
    type Out = N
  }

  implicit def addSuccSwap[N <: Nat, M <: Nat](
    implicit recur: Add[N, Succ[M]]
  ): Aux[Succ[N], M, recur.Out] =
    new Add[Succ[N], M] {
      type Out = recur.Out
    }

  def addCong[N <: Nat, M <: Nat](
    implicit base: Add[N, M]
  ): Add[Succ[N], Succ[M]] = new Add[Succ[N], Succ[M]] {
    type Out = Succ[Succ[base.Out]]
  }

  def addReduceLeft[N <: Nat, M <: Nat](
    implicit higher: Add[Succ[N], M]
  ): Add[N, M] = new Add[N, M] {
    type Out = _0
  }

  def addAssoc[N <: Nat, M <: Nat, O <: Nat, NM <: Nat, MO <: Nat](
    implicit addNM: Aux[N, M, NM],
    addMO: Aux[M, O, MO],
    addNMO: Add[NM, O]
  ): Aux[N, MO, addNMO.Out] = new Add[N, MO] {
    type Out = addNMO.Out
  }
}

object Testbed {
  type _1 = Succ[_0]
  type _2 = Succ[_1]
  type _3 = Succ[_2]
  type _4 = Succ[_3]

  val foo = Add[_3, _1]
  implicitly[foo.Out =:= _4]
}
