package co.technius.typeplayground.myimpl

import cats.evidence.Is

sealed abstract class Vect[N <: Nat, A] {
  type Length = N
  def length: Int = this match {
    case nil: VNil[A] => 0
    case cons: Vect[N, A] => 1 + cons.length
  }

  def ::(head: A): Vect[Succ[Length], A] = VCons(head, this)

  def map[B](f: A => B): Vect[Length, B]
  def append(value: A): Vect[Succ[Length], A]
  def foldLeft[Acc](z: Acc)(f: (Acc, A) => Acc): Acc
  def zipWith[B, C](other: Vect[Length, B])(f: (A, B) => C): Vect[Length, C]
  def unzip[B, C](implicit prf: A Is (B, C)): (Vect[Length, B], Vect[Length, C])

  def zip[B](other: Vect[Length, B]) = zipWith(other)((a, b) => (a, b))
}
final case class VNil[A]() extends Vect[_0, A] {
  override def toString = "Nil"
  def map[B](f: A => B): Vect[_0, B] = VNil()
  def zipWith[B, C](other: Vect[_0, B])(f: (A, B) => C): Vect[_0, C] = VNil()
  def unzip[B, C](implicit prf: A Is (B, C)): (Vect[_0, B], Vect[_0, C]) = (VNil(), VNil())
  def append(value: A): Vect[Succ[Length], A] = VCons(value, VNil())
  def foldLeft[Acc](z: Acc)(f: (Acc, A) => Acc): Acc = z
}
final case class VCons[N <: Nat, A](head: A, tail: Vect[N, A]) extends Vect[Succ[N], A] {
  override  def toString = s"${head.toString} :: ${tail.toString}"
  def map[B](f: A => B): Vect[Length, B] = f(head) :: tail.map(f)
  def zipWith[B, C](other: Vect[Length, B])(f: (A, B) => C): Vect[Length, C] = {
    val VCons(b, bs) = other
    f(head, b) :: tail.zipWith(bs)(f)
  }
  def unzip[B, C](implicit prf: A Is (B, C)): (Vect[Length, B], Vect[Length, C]) = {
    val (hb, hc): (B, C) = prf.coerce(head)
    val (tb, tc) = tail.unzip
    (hb :: tb, hc :: tc)
  }
  def append(value: A): Vect[Succ[Length], A] = head :: tail.append(value)
  def foldLeft[Acc](z: Acc)(f: (Acc, A) => Acc): Acc = tail.foldLeft(f(z, head))(f)
}

object Vect {
  def apply[A](x: A): Vect[Succ[_0], A] = VCons(x, VNil())

  val sample: Vect[Succ[Succ[_0]], Int] = 1 :: 2 :: VNil()
  val foo = sample.map(_ + 1)
  val sample2 = "foo" :: "bar" :: VNil()
  val zipped = sample.zip(sample2)
}
