package co.technius.typeplayground

import shapeless._
import scala.language.higherKinds

/**
  * Writes values as CSV
  */
trait CsvWriter[T] {
  def write(value: T): Seq[String]
}

object CsvWriter {
  def apply[T](f: T => Seq[String]): CsvWriter[T] = new CsvWriter[T] {
    def write(value: T) = f(value)
  }

  def write[T](value: T)(implicit w: CsvWriter[T]): String =
    w.write(value).mkString(",")

  def writeAll[T](values: Seq[T])(implicit w: CsvWriter[T]): String =
    values.map(write(_)).mkString("\n")

  implicit val intCsvWriter: CsvWriter[Int] = CsvWriter(i => Seq(i.toString))
  implicit val doubleCsvWriter: CsvWriter[Double] = CsvWriter(d => Seq(d.toString))
  implicit val stringCsvWriter: CsvWriter[String] = CsvWriter(Seq(_))
  implicit val booleanCsvWriter: CsvWriter[Boolean] = CsvWriter(b => Seq(b.toString))

  /**
    * Instance of CsvWriter for sequences
    */
  implicit def seqWriter[T, F[T] <: Seq[T]](implicit w: CsvWriter[T]): CsvWriter[F[T]] =
    CsvWriter(_.flatMap(w.write(_)))
}
