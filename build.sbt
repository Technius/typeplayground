import Dependencies._

lazy val root = (project in file ("."))
  .settings(
    name := "typeplayground",
    inThisBuild(List(
      organization := "co.technius",
      version      := "0.1.0-SNAPSHOT",
      scalacOptions ++= Seq(
        "-unchecked",
        "-deprecation",
        "-feature"
      )
    ))
  )
  .aggregate(now, next)

lazy val now = (project in file("now"))
  .settings(
    scalaVersion := "2.12.2",
    libraryDependencies ++= Seq(
      shapeless,
      magnolia,
      catsCore,
      scalaTest % Test
    ),
    scalacOptions ++= Seq(
      "-Xfuture",
      "-Yno-adapted-args",
      "-explaintypes"
    )
  )

lazy val next = (project in file("next"))
  .settings(
    scalaVersion := "0.2.0-RC1"
  )
